# Princip jedné odpovědnosti

Tento princip nám říká, že každá třída by měla být zodpovědná za jednu konkrétní věc a veškerá její očekávaná funkčnost by měla být spjata pouze s danou odpovědností.

## Motivace

```cpp
class Journal
{
public:
    explicit Journal(const std::string& aTitle)
        : _title(aTitle) {
    
    }
    
    void addEntries(const string& aEntry) {       
        _entries.push_back(std::to_string(_entries.size() + 1) + ": " + aEntry);
    }
    
    auto getEntries() const {
        return _entries;
    }
    
    void save(const string& aFilename) {
        ofstream ofs(aFilename); 
        
        for(const auto& s : _entries) ofs << s << endl;
    }
    
private:
    std::string _title;
    std::vector<std::string> _entries;
};

int  main() {
    Journal journal{"Dear Diary"};
    journal.addEntries("I ate a bug");
    journal.addEntries("I cried today");
    journal.save("diary.txt");
    
    return EXIT_SUCCESS;
}

```

Výše je uvedený kód třídy `Journal`. I když tento kód na první pohled vypadá dobře, narazíme na problémy při přidávání dalších typů objektů (například `Book` nebo `File`). Každý z těchto objektů by měl mít svou vlastní metodu pro ukládání. To znamená, že pokud budeme chtít změnit způsob, jakým se ukládá například `File`, budeme muset upravit jeho metodu `save`.

## Řešení

```cpp
class Journal {
public:
    explicit Journal(const std::string& aTitle)
        : _title(aTitle) {
    
    }
    
    void addEntries(const string& aEntry) {
        static uint32_t count = 1;
        _entries.push_back(to_string(count++) + ": " + aEntry);
    }
    
    auto getEntries() const {
        return _entries;
    }
    
private:
    std::string _title;
    std::vector<std::string> _entries;
};

struct SavingManager
{
    static void save(const Journal& aJournal, const string& aFilename) {
        std::ofstream ofs(aFilename);
        
        for(auto& s : aJournal.getEntries()) {
            ofs << s << endl;
        }
    }
};

int  main() {
    Journal journal{"Dear Diary"};
    journal.addEntries("I ate a bug");
    journal.addEntries("I cried today");
    
    SavingManager::save(journal, "diary.txt");
    
    return EXIT_SUCCESS;
}
```

Jedním z možných řešení je vytvoření třídy `SavingManager`, která má na starosti pouze ukládání. Tím pádem třída `Journal` se stará jen o zápisy a jejich obsah.

## Výhody principu jedné odpovědnosti

#### Výstižnost

Když třída dělá jen jednu věc, je obvykle kratší a přehlednější, což usnadňuje práci programátorům.

#### Udržitelnost

Jednoduchý kód je méně náchylný k chybám a snáze se udržuje. Pokud má třída jen jednu odpovědnost, je méně pravděpodobné, že se při změnách v jedné části kódu vnese chyba někam jinam. 

#### Možnost opakovaného použití

Pokud má třída více odpovědností a pouze jedna z nich je potřebná v jiné oblasti softwaru, pak ostatní zbytečné odpovědnosti brání opakovanému použití této třídy v jiných kontextech. Když třída má jen jednu odpovědnost, je pravděpodobnější, že bude použitelná v jiných částech programu nebo projektech.

#### Flexibilita a lepší testovatelnost

Třídy s jedinou odpovědností jsou obvykle mnohem snáze testovatelné. Když třída dělá jen jednu věc, můžeme se soustředit na testování pouze jen této jediné věci a nemusíme brát v úvahu další složitosti nebo závislosti. To vede k rychlejšímu psaní testů a vyšší míře pokrytí testy.

## Závěr

Výše uvedený příklad v C++ ilustruje princip **Single Responsibility**, který zdůrazňuje, že každá třída by měla mít jen jednu zodpovědnost. Dodržování tohoto principu vede k větší výstižnosti, udržitelnosti, možnosti opakovaného použití třídy a lepší testovatelnost kódu.
