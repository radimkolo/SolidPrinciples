# Princip otevřenosti a uzavřenosti 

Tento princip nám říká, že třídy by měly být otevřeny pro rozšíření, ale uzavřeny pro úpravy. To doslova znamená, že bysme měli být schopni rozšířit chování třídy tak, aniž bychom ji upravovali.

## Motivace

Předpokládejme třídu `Account`, která má na starost několik typů bankovních účtů jako jsou `Savings Accounts`, `Credit Card Accounts` a `Fixed Deposit Accounts`. 

```cpp
class Account {
public: 
    Account(double aBalance, int aTypeOfAccount)
        : _balance(aBalance)
        , _typeOfAccount(aTypeOfAccount) {
        
    }

    void deposit(double amount) {
        _balance += amount;
    }

    void withdraw(double amount) {
        _balance -= amount;
    }

    double getBalance() const {
        return _balance;
    }

    double calculateInterest() const {
        if(_typeOfAccount == 1) {
        
        }
        else if(_typeOfAccount == 2) {
        
        } 
        // ...
    }

private:
    double _balance;
    int _typeOfAccount;
};
```

Zde můžeme pozorovat to, že při výpočtu úroků se výpočet pro jednotlivé účty může lišit a musíme proto typy účtů rozlišovat. To znamená, že kdybychom chtěli přidat podporu pro další účet, museli bychom upravit metodu `calculateInterest()` a tím tedy modifikovat třídu `Account`. Toto však porušuje základ principu otevřenosti a uzavřenosti, konkrétně uzavřenost třídy vůči modifikaci.

Předpokládejme, že tento kód je již plně otestován a produkčně nasazen. Pokud tuto třídu upravíme, znamená to, že musíme spustit testy pro všechny typy účtů, abychom se ujistili, že nedošlo k regresi.

Také pokud nový typ účtu přinese nový způsob výběru nebo vkladu, znamená to, že musíme upravit i tyto metody.

## Řešení

Nejlepším řešením je využití dědičnosti a polymorfismu.

Vytvoříme abstraktní třídu (rozhraní) `Account` a pro každý typ účtu odvodíme specifickou třídu, která implementuje rozhraní `Account`. 

```cpp
class Account {
public:
    virtual void deposit(double aAmount) = 0;
    virtual void withdraw(double aAmount) = 0;
    virtual double getBalance() = 0;
    virtual double calculateInterest() = 0;
}; 
```

##### SavingsAccount

```cpp
class SavingsAccount : public Account {
public:
    SavingsAccount(double aBalance)
        : _balance(aBalance) {

    }
    
    void deposit(double aAmount) override {
        _balance += aAmount;
    }
    
    void withdraw(double aAmount) override {
        _balance -= aAmount;
    }
    
    double getBalance() override {
        return _balance;
    }
    
    double calculateInterest() override {
        return _balance * 0.04; // 4% interest rate 
    }

private:
    double _balance; 
};
```

##### FixedDepositAccount

```cpp
class FixedDepositAccount : public Account {
public :
    FixedDepositAccount(double aBalance, int aTime)
        : _balance(aBalance)
        , _time(aTime) {

    }
    
    void deposit(double aAmount) override {
        _balance += aAmount;
    }
    
    void withdraw(double aAmount) override {
        // Logic to withdraw after certain time
        _balance -= aAmount;
    }
    
    double getBalance() override {
        return bala_balancence;
    }
    
    double calculateInterest() override {
        return _balance * 0.08; // 8% interest rate 
    }

private:
    double _balance; 
    int _time;
};
```

##### CreditCardAccount

```cpp
class CreditCardAccount : public Account {
public:
    CreditCardAccount(double aBalance, double aLimit)
    : balance(aBalance)
    , _limit(aLimit) {

    }

    void deposit(double aAmount) override {
        _balance -= aAmount;
    }

    void withdraw(double aAmount) override {
        _balance += aAmount; // credit card withdrawal
    }

    double getBalance() override {
        return _balance;
    }

    double calculateInterest() override {
        return _balance * 0.18; // 18% interest rate
    }

    void increaseLimit(double addon) {
        _limit += addon;
    }

private:
    double _balance;
    double _limit;
};
```

Pokud se podíváme na výše uvedený kód, máme třídy `CreditCardAccount`, `FixedDepositAccount` a `SavingsAccount`, které implementují abstraktní třídu `Account` a přepisují jejich metody vlastní funkčností.

Funkčnost výpočtu úroku, výběru peněz atd. se od sebe liší. Dědičnost a polymorfismus hrají při dosažení tohoto oddělení zásadní roli.

Tím, že v abstraktní třídě definujeme společné rozhraní odvozeným třídám, umožníme implementovat jejich vlastní specifické chování. Napíšeme tak kód, který je znovupoužitelný a snadněji se testuje.

Tento návrh se řídí principem otevřenosti a uzavřenosti, protože nyní můžeme přidat jakýkoli nový typ účtů vytvořením nové odvozené třídy implementující rozhraní z abstraktní třídy `Account`. Tato nová třída pak implementuje svou vlastní specifickou funkčnost, aniž by bylo potřeba upravovat stávající třídu `Account` nebo jakoukoli jinou třídu. Díky tomu je program rozšiřitelnější a snadněji se udržuje.

## Výhody principu otevřenosti a uzavřenosti

#### Rozšiřitelnost

“When a single change to a program results in a cascade of changes to dependent modules, that program exhibits the undesirable attributes that we have come to associate with ‘bad’ design. The program becomes fragile, rigid, unpredictable and unreusable. The open-closed principle attacks this in a very straightforward way. It says that you should design modules that never change. When requirements change, you extend the behaviour of such modules by adding new code, not by changing old code that already works.”
— Robert Martin

#### Udržitelnost

Hlavní výhodou tohoto přístupu je, že rozhraní přináší další úroveň abstrakce, která umožňuje volné propojení. Implementace rozhraní jsou na sobě nezávislé a nemusí sdílet žádný kód. Tím pádem můžeme snadno vyhovět stále se měnícím požadavkům klienta.

#### Flexibilita

Princip otevřenosti a uzavřenosti se také vztahuje k architektuře pluginů a middleware. V tomto případě je základním softwarovým prvkem pouze základní funkčnost aplikace. V případě pluginů aplikace obsahuje pouze základní nebo jádrový modul, který lze rozšířit o novou funkcionalitu prostřednictvím definovaných rozhraní.

#### Snížení rizika chyb

Při dodržování tohoto principu se nový kód přidává místo úpravy stávajícího kódu. To znamená, že existující funkcionalita, která byla již dříve otestována, zůstává nedotčena. Snižuje se tak riziko vzniku nových chyb ve stávajícím kódu.

#### Zvýšená produktivita

Rychlejší a snadnější implementace nových funkcí bez nutnosti přepisování nebo úprav stávajícího kódu může zvýšit produktivitu vývojových týmů.

#### Modularita

Dodržování principu otevřenosti a uzavřenosti často vede k modularitě kódu, což usnadňuje jeho pochopení, údržbu a rozšíření.

## Závěr

Mějme na paměti, že třídy nemohou být nikdy zcela uzavřeny. Vždy se vyskytnou nepředvídané změny, které vyžadují jejich úpravu. Pokud však lze změny předvídat, pak máme ideální příležitost aplikovat princip otevřenosti a uzavřenosti, abychom byli připraveni na změny požadavků v budoucnosti. 