# Liskovové princip substituce

Tento princip říká, že odvozený typ je nahraditelný svým rodičovským typem, aniž by bylo nutné změnit vlastnosti rodičovského nebo odvozeného typu.

V kontextu jazyka C++ to znamená, že funkce, které používají ukazatele/odkazy na bázové třídy, musí být schopny je nahradit za své odvozené třídy. Princip substituce závisí na správném použití dědičnosti.

## Motivace

Předpokládejme, že máme navrhnout hierarchii ptáků, ve které potřebujeme implementovat ptáky jako `Penguin`, `Crow` atd.

```cpp
class Bird {
public:
    virtual void fly() = 0;
};

class Crow : public Bird {
public:
    void fly() override {
        cout << "Crow is flying" << endl;
    }
};

class Penguin : public Bird {
public:
    void fly() override {
        throw runtime_error("Penguins can't fly!");
    }
};
```

Ve výše uvedeném příkladu máme třídu s názvem `Bird`, což je abstraktní třída s čistě virtuální metodou `fly()`.

Dále máme dvě odvozené třídy `Crow` a `Penguin`, které dědí třídu `Bird`. Protože tučňák neumí létat, metoda `fly()` vyhodí výjimku `runtime_error`.

Řekněme, že máme funkci, která přijímá jako parametr objekt typu `Bird` a zavolá na něm metodu `fly()`.

```cpp
void makeBirdFly(const Bird& aBird) {
    aBird.fly();
}
```

Pokud tuto funkci zavoláme nad objektem `Crow`, bude fungovat podle očekávání, ale pokud ji zavoláme nad objektem `Penguin`, dojde k vyvolání výjimky. Tímto je porušen princip substituce, protože nahrazení objektu `Penguin` objektem `Bird` vede k rozdílnému chování. Také redukujeme jednu funkcionalitu z odvozené třídy `Penguin`.

Z nedodržení principů substituce plyne mnoho nevýhod:

* Bude velmi obtížné zachovat hierarchii mezi třídami. Mohlo by dojít k mnoha problémům při běhu a podivnému chování.
* Unit testy bázové třídy nebudou nikdy předány odvozené třídě, což ztěžuje testování kódu. 
* Může se vyskytnout spousta podmíněných kontrol.

## Řešení

K vyřešení těchto problémů můžeme použít vhodnější hierarchii tříd. Můžeme přidat další odvozenou třídu s názvem `FlyingBird`, která reprezentuje ptáky, kteří umí létat.

```cpp
class Bird {
public:
    virtual bool canFly() = 0;
};

class FlyingBird : public Bird {
public:
    bool canFly() override {
        return true;
    }

    virtual void fly() = 0;
};

class Crow : public FlyingBird {
public:
    void fly() override {
        std::cout << "Crow is flying" << std::endl;
    }
};

class Penguin : public Bird {
public:
    void canFly() override {
        return false;
    }
};
```

Nyní upravíme naši funkci `makeBirdFly()` tak, aby využívala tuto novou hierarchii.

```cpp
void makeBirdFly(const FlyingBird& aBird) {
    aBird.fly();
}
```

Pokud nyní zavoláme funkci `makeBirdFly()` nad objektem `Crow`, bude fungovat podle očekávání. Pokud se ji pokusíme zavolat nad objektem `Penguin`, dostaneme kompilační chybu, protože třída `Penguin` nedědí od třídy `FlyingBird`.

Tímto je zajištěno, že funkci `makeBirdFly()` lze předat pouze ptáka, který umí létat, což zabraňuje neočekávanému chování a je zachován princip substituce.

## Výhody Liskovové principu substituce

#### Kompatibilita

Díky tomuto principu můžeme upravovat a rozšiřovat svůj software, aniž by došlo k narušení stávající funkčností. To znamená méně problémů v průběhu životního cyklu softwaru.

#### Jednoduchost a bezpečnost práce s typy

Pomáhá zjednodušit práci s různými objekty, protože všechny odvozené objekty musí splňovat stejná pravidla. To snižuje riziko neočekávaných chyb při práci s různými typy objektů.

#### Lepší organizace a opětovné využití kódu

Kód dodržující tento princip, je čistší, lépe organizovaný a umožňuje snadnější opětovné použití v různých částech programu.

## Závěr

Pokud byste si chtěli přečíst něco o Barbaře Liskové, která s tímto principem přišla, můžete [zde](https://en.wikipedia.org/wiki/Barbara_Liskov).