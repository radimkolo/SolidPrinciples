# Princip inverze závislotí 

Tento princip nás vede k tomu, abychom závislosti v našem kódu formovali tak, aby třídy vyšší úrovně nebyly závislé na třídách nižší úrovně. Obě dvě úrovně by měly být závislé jen  na abstrakcích.

Princip inverze závislostí může být formulován dvěma klíčovými body:

1. Moduly vyšší úrovně by neměly záviset na modulech nižší úrovně. Oba by měly záviset na abstrakcích.

2. Abstrakce by neměla záviset na detailech. Detaily by měly záviset na abstrakcích.

## Motivace

Předpokládejme, že máme třídu `LightBulb`, která má metody pro vypnutí a zapnutí. Poté máme třídu `ElectricPowerSwitch`, která tuto žárovku ovládá.

```cpp
class LightBulb {
public:
    void turnOn() {
        std::cout << "Light bulb on." << std::endl;
    }

    void turnOff() {
        std::cout << "Light bulb off." << std::endl;
    }
};

class PowerSwitch {
public:
    PowerSwitch(LightBulb& aLightBulb)
        : _lightBulb(aLightBulb) {

    }
    
    void press() {
        if (on_) {
            _lightBulb.turnOff();
            _on = false;
        }
        else {
            _lightBulb.turnOn();
            _on = true;
        }
    }

private:
    LightBulb& _lightBulb;
    bool _on = false;
};

int main() {
    LightBulb bulb;
    PowerSwitch powerSwitch(bulb);

    powerSwitch.press();
    powerSwitch.press();

    return EXIT_SUCCESS;
}
```

V tomto případě je `PowerSwitch` přímo závislý na `LightBulb`. To znamená, že `PowerSwitch` může fungovat jen pro `LightBulb` a ne pro další zařízení, která by šla ovládat stejným způsobem. Například `Kettle` nebo `FoodProcessor`.

## Řešení

Pro vyřešení tohoto problému musíme vytvořit rozhraní `ISwitchable` použitelné pro všechna zařízení, která budou implementovat metody pro zapnutí a vypnutí.

```cpp
class ISwitchable {
public:
    virtual void turnOn() = 0;
    virtual void turnOff() = 0;
};

class LightBulb final : public ISwitchable {
    void turnOn() override {
        std::cout << "Turn on light bulb" << std::endl;
    }

    void turnOff() override {
        std::cout << "Turn off light bulb" << std::endl;
    }
};

class Kettle final : public ISwitchable {
    void turnOn() override {
        std::cout << "Turn on kettle" << std::endl;
    }

    void turnOff() override {
        std::cout << "Turn off kettle" << std::endl;
    }
};

class PowerSwitch {
public:
    PowerSwitch(ISwitchable& aSwitchable)
        : _switchable(aSwitchable) {

    }

    void press() {
        if (_on) {
            _switchable.turnOff();
            _on = false;
        }
        else {
            _switchable.turnOn();
            _on = true;
        }
    }

private:
    ISwitchable& _switchable;
    bool _on = false;
};

int main() {
    LightBulb lightBulb;
    PowerSwitch lightBulbSwitch(lightBulb);

    lightBulbSwitch.press();
    lightBulbSwitch.press();
  
    Kettle kettle;
    PowerSwitch kettleSwitch(kettle);

    kettleSwitch.press();
    kettleSwitch.press();

    return EXIT_SUCCESS;
}
```

Nyní není `PowerSwitch` závislý jen na konkrétním zařízení, ale podporuje kterékoliv zařízení, které dědí rozhraní `ISwitchable`.

## Výhody principu iverze závislostí

#### Opakované použití

Princip účinně snižuje závislosti mezi různými částmi kódu. Získáme tak znovu použitelný kód.

#### Udržovatelnost

Je také důležité zmínit, že změna již implementovaných modulů je riskantní. Tím, že jsme závislí na abstrakci a ne na konkrétní implementaci, toto riziko snižujeme, protože nemusíme měnit vysokoúrovňové moduly. Princip inverze závislostí nám při správném použití poskytuje flexibilitu a stabilitu na úrovni celé architektury naší aplikace.

## Závěr

Nepoužívejme přímo konkrétní objekt, pokud k tomu nemáme pádný důvod. Místo toho použijme abstrakci.
