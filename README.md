# S.O.L.I.D Principles v C++

## Úvod

Při programování je důležité myslet nejen na aktuální potřeby, ale i na to, jak bude kód fungovat v budoucnosti. Chceme, aby byl kód srozumitelný, snadno udržitelný a flexibilní. Právě k tomu nám pomáhají **S.O.L.I.D** principy.

- [x] **S** - [Single Responsibility](SingleResponsibility/README.md) (Princip jedné odpovědnosti)
- [x] **O** - [Open-Closed](OpenClosed/README.md)                     (Princip otevřenosti a uzavřenosti)
- [x] **L** - [Liskov Substitution](LiskovSubstitution/README.md)     (Liskovové princip zaměnitelnosti)
- [x] **I** - [Interface Segregation](InterfaceSegregation/README.md) (Princip oddělení rozhraní)
- [x] **D** - [Dependency Inversion](DependencyInversion/README.md)   (Princip inverze závislostí)

V tomto repozitáři si ukážeme, jak tyto principy aplikovat v jazyce C++. Vybral jsem si C++, protože většina dostupných zdrojů ukazuje příklady v jiných jazycích, například v Javě. Já sám v C++ programuji, takže to dělám hlavně kvůli sobě.

##### Poznámka

Nedávno jsem viděl super [video](https://www.youtube.com/watch?v=q1qKv5TBaOA), kde jsou SOLID Principles probírány obecněji, ale také bych řekl, že i o něco srozumitelněji. Doporučuju si jej pustit.
