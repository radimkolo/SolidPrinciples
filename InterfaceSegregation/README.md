# Princip oddělení rozhraní

Tento princip nám říká, že by klienti neměli být nuceni být závislí na rozhraních, která nepoužívají.

Zjednodušeně řečeno, rozhraní by mělo obsahovat pouze metody, které jsou relevantní pro klienty, kteří jej používají, a neměli by být nuceni implementovat metody, které nepotřebují.

## Motivace

Předpokládejme, že máme navrhnout systém pro tiskárny, ve kterém musíme implementovat různé typy tiskáren, jako je `LaserPrinter`, `InkJetPrinter`, `LEDPrinter`, `3DPrinter`.

Můžeme mít abstraktní třídu pro `IPrinter`, která bude implementovat metody jako `Print`, `Scan` a `Fax`.

```cpp
class IPrinter {
public:
    virtual void print() = 0;
    virtual void scan() = 0;
    virtual void fax() = 0;
};
```

Nyní implementujme některé typy tiskáren.

##### InkJetPrinter

```cpp
class InkJetPrinter : public IPrinter {
  public:
    void print() override {
        std::cout << "InkJet Printer Printing" << std::endl;
        // Implementation of print for InkJet printer
    }

    void scan() override {
        std::cout << "InkJet Printer Scanning" << std::endl;
        // Implementation of scan for InkJet printer
    }

    void fax() override {
        std::cout << "InkJet Printer Faxing" << std::endl;
        // Implementation of fax for InkJet printer
    }
};
```

##### LaserPrinter

```cpp
class LaserPrinter : public IPrinter {
public:
    void print() override {
        std::cout << "Laser Printer Printing" << std::endl;
        // Implementation of print for laser printer
    }
    
    void scan() override {
        std::cout << "Laser Printer Scanning" << std::endl;
        // Implementation of scan for laser printer
    }

    void fax() override {
        throw std::runtime_error("Laser Printer Can't Fax");
    }
};
```

Třída `LaserPrinter` metodu `fax()` nepotřebuje, ale je nucena ji implementovat, protože musí dodržet rozhraní `IPrinter`.

Tím je porušen princip oddělení rozhraní, protože třída `LaserPrinter` je nucena implementovat metodu `fax()`, i když nepodporuje tuto funkcionalitu.

## Řešení

Abychom tento problém vyřešili, můžeme vytvořit samostatná rozhraní pro metody `print()`, `scan()` a `fax()`.

```cpp
class IPrintable {
public:
    virtual void print() = 0;
};
 
class IScannable {
public:
    virtual void scan() = 0;
};

class IFaxable {
public:
    virtual void fax() = 0;
};
```

Nyní implementujme třídy s rozhraními, která jsou relevantní pouze pro ně.

##### InkJetPrinter

```cpp
class InkJetPrinter : public IPrintable, public IScannable, public IFaxable {
public:
    void print() override {
        std::cout << "InkJet Printer Printing" << std::endl;
        // Implementation of print for InkJet printer
    }

    void scan() override {
        std::cout << "InkJet Printer Scanning" << std::endl;
        // Implementation of scan for InkJet printer
    }

    void fax() override {
        std::cout << "InkJet Printer Faxing" << std::endl;
        // Implementation of fax for InkJet printer
    }
};
```

##### LaserPrinter

```cpp
class LaserPrinter : public IPrintable, public IScannable {
public:
    void print() override {
        std::cout << "Laser Printer Printing" << std::endl;
        // Implementation of print for laser printer
    }
    
    void scan() override {
        std::cout << "Laser Printer Scanning" << std::endl;
        // Implementation of scan for laser printer
    }
};
```

Nyní musí tiskárna `LaserPrinter` implementovat pouze metody, které jsou relevantní pro její funkčnost. Tím je nyní dodržen princip oddělení rozhraní, protože třídy nejsou nuceny implementovat nepotřebné metody.

## Poznatky

**Udržujme rozhraní soudržná**: Rozhraní by měla obsahovat pouze sadu souvisejících vlastností nebo metod, které jsou vyžadovány klienty, kteří je používají. Rozhraní by neměla být přeplněna zbytečnými nebo nesouvisejícími metodami. Metody by měly přispívat k jedinému dobře definovanému účelu.

**Rozhraní specifická pro klienta**: Rozhraní by měla být navržena se zaměřením na požadavky klientů, kteří je používají.

**Vyhněte se znečištění rozhraní**: Pokud budeme implementovat zbytečné metody, znečistíme tím třídu a vytvoříme zbytečné závislosti.

## Výhody principu oddělení rozhraní

#### Zaměření na specifičnost

Rozhraní, která jsou navržena tak, aby sloužila konkrétním potřebám, umožňují lepší a jasnější komunikaci mezi komponentami systému.

#### Snížení závislosti

Tím, že komponenty nejsou nuceny záviset na metodách, které nepotřebují, se snižuje riziko nežádoucích vedlejších účinků při změnách v rozhraní.

#### Flexibilita a snadná rozšiřitelnost
Menší a specifická rozhraní mohou být snadněji rozšiřována a přizpůsobována podle potřeb bez ovlivnění stávajících implementací.

#### Srozumitelnost
Specifická rozhraní s menším počtem metod jsou často jednodušší na pochopení, což usnadňuje orientaci v kódu a snižuje chybovost.

#### Snadnější údržba

Při změně nebo aktualizaci jednoho rozhraní nemusíme provádět změny v mnoha třídách, což vede k menším rizikům a jednodušší údržbě.

#### Optimalizace pro různé potřeby
Různé komponenty systému mohou mít různé potřeby, a díky rozděleným rozhraním můžeme lépe a efektivněji reagovat na tyto specifické potřeby.

## Závěr

Přestože velká rozhraní představují potenciální problém, princip oddělení rozhraní se netýká velikosti rozhraní. Jde spíše o to, zda třídy používají metody rozhraní, na kterých jsou závislé.
